solution: https://lukeeastwood.gitlab.io/pathfinder

Enclosed in this repository is my submission for the below Javascript exercise (original source: https://github.com/cloudreach/react-questions/tree/master/question4).
The code for calculating the shortest path between two points can be found in `src/pathfind.js` with UI code found in `main.js`.
I've utilised Facebook's static type checking library 'Flow' to add the safety and greater level of return certainty associated with using types. Babel is being used to strip out the type annotations used by Flow, and to drop the JS version to something that any current browser should be able to run. Browserify is being used to bundle up the source code and handle any `require` statements.
Unit testing has been done using Jasmine (two of the scenarios: 'start and end the same' and 'example case' have been pre-provided) to test each non-trivial function within `pathfind.js`.
These tests are being run on each commit to the server using GitLab's CI offerings and upon passing these tests successfully, the project is continuously deployed to a GitLab Pages page under this repo. 

# Cloudreach React Question 4

This is a question that is about general JS knowledge, not to do with React.

Imagine representing a grid-shaped game map as a 2-dimensional array. Each value of this array is
boolean `true` or `false` representing whether that part of the map is passable (a floor) or blocked
(a wall). Write a function `function pathfind(A, P, Q) { }`that takes such an array `A` and 2 points
`P` and `Q` in a 2-dimensional space each represented by an array of two elements `[x, y]` from the top left corner of the map (in the example below, `P = [1, 0]` and `Q = [2, 3]`), and returns the distance of the shortest path between those
points, respecting the walls in the map.

eg. Given the map (where '.' is passable - `true`, and '#' is blocked - `false`)
```
.P...
.###.
.....
..Q..
.....
```

then `pathfind(A, P, Q)` should return `6`.

## What to do

1. Implement the `pathfind` function in `pathfind.js`.
2. Feel free to add further test cases to `spec/pathfind_spec.js`.

## Running the tests
Tests have been implemented using Jasmine and can be run by running `npm test`
