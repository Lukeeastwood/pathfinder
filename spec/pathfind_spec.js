const {
  pathfind,
  calculateManhattanDistance,
  getNeighbours,
  getInsertPosition
} = require('../lib/pathfind');

const exampleNode = {
  position: { x: 1, y: 1 },
  predecessor: null,
  h: 5,
  g: 4,
  f: 9
};

describe('Pathfind', () => {
  it('start and end the same', async () => {
    const A = [
      [true, true, true, true, true],
      [true, false, false, false, true],
      [true, true, true, true, true],
      [true, true, true, true, true],
      [true, true, true, true, true]
    ];
    const P = [0, 0];
    const Q = [0, 0];
    let path = await pathfind(A, P, Q);
    expect(path.g).toBe(0);
  });

  it('example case', async () => {
    const A = [
      [true, true, true, true, true],
      [true, false, false, false, true],
      [true, true, true, true, true],
      [true, true, true, true, true],
      [true, true, true, true, true]
    ];
    const P = [1, 0];
    const Q = [2, 3];
    let path = await pathfind(A, P, Q);
    expect(path.g).toBe(6);
  });

  it('calculateDistance - Manhattan', () => {
    expect(calculateManhattanDistance({ x: 10, y: 12 }, { x: 20, y: 14 })).toBe(12);
    expect(calculateManhattanDistance({ x: 5, y: 12 }, { x: 2, y: 8 })).toBe(7);
    expect(calculateManhattanDistance({ x: 1, y: 1 }, { x: 1, y: 1 })).toBe(0);
  });

  it('getNeighbours - no neighbours', () => {
    const currentGrid = [
      [true, false, true],
      [false, true, false],
      [true, false, true]
    ];
    expect(getNeighbours({ ...exampleNode }, currentGrid).length).toBe(0);
  });

  it('getNeighbours - four neighbours', () => {
    const currentGrid = [
      [true, true, true],
      [true, true, true],
      [true, true, true]
    ];
    expect(getNeighbours({ ...exampleNode }, currentGrid).length).toBe(4);
  });

  it('getNeighbours - two neighbours', () => {
    const currentGrid = [
      [true, false, true],
      [true, true, false],
      [true, true, true]
    ];
    expect(getNeighbours({ ...exampleNode }, currentGrid).length).toBe(2);
  });

  it('getInsertPosition - empty list', () => {
    expect(getInsertPosition([], { ...exampleNode, f: 7 })).toBe(0);
  });

  it('getInsertPosition - end of list', () => {
    expect(getInsertPosition([{ ...exampleNode, f: 3 }, { ...exampleNode, f: 6 }, { ...exampleNode, f: 19 }], { ...exampleNode, f: 70 })).toBe(3);
  });

  it('getInsertPosition - middle of list', () => {
    expect(getInsertPosition([{ ...exampleNode, f: 3 }, { ...exampleNode, f: 6 }, { ...exampleNode, f: 19 }], { ...exampleNode, f: 7 })).toBe(2);
  });
  // You can add further tests here
});
