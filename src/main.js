//@flow
import '@babel/polyfill';
import { findShortestPath } from './pathfind.js';

const cellClass = { start: 'start', end: 'end', wall: 'wall', frontier: 'frontier', path: 'path', current: 'current'};

let start, end, grid;

document.getElementById('grid').addEventListener('click', event => {
  if (event.target.nodeName !== 'TD') return;
  let cell = event.target;
  let x = cell.getAttribute('x');
  let y = cell.getAttribute('y');
  if (start.x === x && start.y === y || end.x === x && end.y === y) return;
  if (cell.className === 'wall') {
    cell.classList.remove('wall');
    grid[cell.getAttribute('y')][cell.getAttribute('x')] = true;
  } else {
    cell.classList.add('wall');
    grid[cell.getAttribute('y')][cell.getAttribute('x')] = false;
  }
});

document.getElementById('controls').addEventListener('input', event => {
  switch (event.target.id) {
    case 'sx':
    case 'sy':
      updateStartPosition();
      break;
    case 'ex':
    case 'ey':
      updateEndPosition();
  }
});

function updateStartPosition() {
  let x: number, y: number;
  x = parseInt(document.getElementById('sx').value, 10);
  y = parseInt(document.getElementById('sy').value, 10);
  let old = document.getElementsByClassName('start')[0];
  removeClass(parseInt(old.getAttribute('x'), 10), parseInt(old.getAttribute('y'), 10));
  assignClass(x, y, cellClass.start);
  grid[y][x] = true;
  start = { x: x, y: y}
}

function updateEndPosition() {
  let x: number, y: number;
  x = parseInt(document.getElementById('ex').value, 10);
  y = parseInt(document.getElementById('ey').value, 10);
  let old = document.getElementsByClassName('end')[0];
  removeClass(parseInt(old.getAttribute('x'), 10), parseInt(old.getAttribute('y'), 10));
  assignClass(x, y, cellClass.end);
  grid[y][x] = true;
  end = { x: x, y: y };
}


function assignClass(x: number, y: number, classToAssign: string): void {
  let element = document.querySelectorAll(`[x="${x}"][y="${y}"]`)[0];
  element.classList = [];
  element.classList.add(classToAssign);
}

function addCellValues(x: number, y: number, node: Node) {
  let element = document.querySelectorAll(`[x="${x}"][y="${y}"]`)[0];
  element.innerHTML = `<div>f: ${node.f}</div><div>g: ${node.g}</div><div>h: ${node.h}</div>`;
}

function addCellInfo(node: Node, cellClass: string) {
  assignClass(node.position.x, node.position.y, cellClass);
  addCellValues(node.position.x, node.position.y, node);
}

function removeClass(x: number, y: number) {
  let element = document.querySelectorAll(`[x="${x}"][y="${y}"]`)[0];
  element.classList = [];
}

function draw(frontier: Array<Node>, currentNode: Node): void {
  let cells = document.getElementsByTagName('td');
  for (let cell of cells) if (cell.className !== cellClass.wall) cell.classList = [];
  frontier.forEach(node => addCellInfo(node, cellClass.frontier));
  addCellInfo(currentNode, cellClass.current);
  while (currentNode.predecessor !== null && currentNode.predecessor !== undefined) {
    currentNode = currentNode.predecessor;
    addCellInfo(currentNode, cellClass.path);
  }
  return new Promise(resolve => setTimeout(() => resolve(), 250));
}

function noPath(): void {
  alert('No valid path!');
}


async function init(): void {
  let delay: boolean;
  delay = document.getElementById('delay').checked;
  let path: Node = await findShortestPath(grid, start, end, delay ? draw : null);
  if (path.g === -1) {
    noPath();
    return;
  }
  do {
    addCellInfo(path, cellClass.path);
    path = path.predecessor;
  } while(path !== null && path !== undefined);
  removeClass(start.x, start.y);
  removeClass(end.x, end.y);
  assignClass(start.x, start.y, cellClass.start);
  assignClass(end.x, end.y, cellClass.end);
}

function clearGrid(): void {
  let cells = document.querySelectorAll('#grid td');
  for (let cell of cells) {
    cell.classList = [];
    cell.innerHTML = '';
  }
  start = { x: 0, y: 0 };
  end = { x: 9, y: 9 };
  assignClass(0, 0, cellClass.start);
  assignClass(9, 9, cellClass.end);
  grid = [
    [true, true, true, true, true, true, true, true, true, true],
    [true, true, true, true, true, true, true, true, true, true],
    [true, true, true, true, true, true, true, true, true, true],
    [true, true, true, true, true, true, true, true, true, true],
    [true, true, true, true, true, true, true, true, true, true],
    [true, true, true, true, true, true, true, true, true, true],
    [true, true, true, true, true, true, true, true, true, true],
    [true, true, true, true, true, true, true, true, true, true],
    [true, true, true, true, true, true, true, true, true, true],
    [true, true, true, true, true, true, true, true, true, true],
  ];
}

window.init = init;
window.noPath = noPath;
window.clearGrid = clearGrid;

clearGrid();
