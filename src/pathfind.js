// @flow
import '@babel/polyfill';

let currentGrid: Array<Array<boolean>>;
let currentGoal: Point = {x: 0, y: 0};

type Node = {
  predecessor: ?Node,
  position: Point,
  g: number,
  h: number,
  f: number
};

type Point = {
  x: number,
  y: number
}

function getNeighbours(currentNode: Node, grid: Array<Array<boolean>>): Array<Node> {
  let neighbours: Array<Node> = [];
  let checkAndAddNeighbour = (p: Point) => grid[p.y][p.x] ? neighbours.push(generateNode(p, currentNode)) : null;
  if (currentNode.position.y > 0) //up
    checkAndAddNeighbour({ x: currentNode.position.x, y: currentNode.position.y - 1 });
  if (currentNode.position.x < grid[0].length - 1) //right
    checkAndAddNeighbour({ x: currentNode.position.x + 1, y: currentNode.position.y });
  if (currentNode.position.y < grid[0].length - 1) //bottom
    checkAndAddNeighbour({ x: currentNode.position.x, y: currentNode.position.y + 1 });
  if (currentNode.position.x > 0) //left
    checkAndAddNeighbour({ x: currentNode.position.x - 1, y: currentNode.position.y });
  return neighbours;
}

function getInsertPosition(insertInto: Array<Node>, toInsert: Node): number {
  let pos: number = 0;
  while (insertInto[pos] !== undefined && insertInto[pos].f < toInsert.f) pos++;
  return pos;
}

function generateNode(position: Point, predecessor: ?Node): Node {
  let h = calculateHeuristic(position, currentGoal);
  let g = predecessor !== undefined ? predecessor.g + 1 : 0;
  let f = g + h;
  return {
    predecessor: predecessor,
    position: position,
    g: g,
    h: h,
    f: f
  };
}

function calculateHeuristic(start: Point, end: Point): number {
  return calculateManhattanDistance(start, end);
}

function calculateManhattanDistance(start: Point, end: Point): number {
  return Math.abs(start.x - end.x) + Math.abs(start.y - end.y);
}

function areEqualPoints(i: Point, j: Point): boolean {
  return i.x === j.x && i.y === j.y;
}

async function findShortestPath(grid: Array<Array<boolean>>, start: Point, end: Point, redraw: (frontier: Array<Node>, currentNode: Node) => void): Node {
  currentGoal = end;
  currentGrid = grid;
  let frontier: Array<Node> = [generateNode(start)];
  let evaluated: Array<Node> = [];
  let current: Node;
  let delay: boolean = redraw !== null;
  while (frontier.length) {
    current = frontier.shift();
    if (delay) await redraw(frontier, current);
    if (current.h === 0) return current; //finished
    getNeighbours(current, currentGrid)
      .filter(f => !evaluated.some(s => areEqualPoints(s.position, f.position)))
      .forEach(n => {
        let replaceAtIndex = frontier.findIndex(fi => areEqualPoints(fi.position, n.position));
        if (replaceAtIndex !== -1) {
          if (frontier[replaceAtIndex].f > n.f) frontier[replaceAtIndex] = n;
          else return;
        }
        frontier.splice(getInsertPosition(frontier, n), 0, n)
      });
    evaluated.push(current);
  }
  return { g: -1 };
}

function pathfind(A: Array<Array<boolean>>, P: Array<number>, Q: Array<number>) {
  let path: Node = findShortestPath(A, { x: P[0], y: P[1] }, { x: Q[0], y: Q[1] }, null);
  if (path.g === -1) {
    console.log('no valid path.');
    return;
  }
  return path;
}

module.exports.findShortestPath = findShortestPath;
module.exports.pathfind = pathfind;
module.exports.calculateManhattanDistance = calculateManhattanDistance;
module.exports.getNeighbours = getNeighbours;
module.exports.getInsertPosition = getInsertPosition;
